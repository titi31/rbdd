extern crate rbdd;
extern crate diesel;

use self::diesel::prelude::*;
use self::rbdd::*;
use self::models::Post;
use std::env::args;

fn main() {
    use rbdd::schema::posts::dsl::{posts, published};
    use rbdd::schema::posts::dsl::*;

    let id1 = args().nth(1).expect("publish_post requires a post id")
        .parse::<i32>().expect("Invalid ID");
    let connection = establish_connection();

    diesel::update(posts.find(id1))
        .set(published.eq(true))
        .execute(&connection)
        .expect(&format!("Unable to find post {}", id1));
     let post=posts.filter(id.eq(id1)).limit(5)
     .load::<Post>(&connection)
     .expect("Error loading posts");   
     for p in post{
    println!("Published post {}", p.title);}
}